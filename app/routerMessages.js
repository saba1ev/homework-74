const express = require('express');
const db = require('../fileDb');

const router = express.Router();

router.get('/', (req,res)=>{
  res.send(db.getMessages())
});

router.post('/', (req,res)=>{
  db.addMessage(req.body.message);
  res.send('Message send');
});


module.exports = router;