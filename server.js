const express = require('express');
const messages = require('./app/routerMessages');
const db = require('./fileDb');

const app = express();
db.init();
const port = 8000;

app.use(express.json());

app.use('/messages', messages);

app.listen(port, ()=>{
  console.log('Port: ' + port);
})