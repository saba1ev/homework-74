const fs = require('fs');

let messages = [];

module.exports = {
  init(){
    try {
      fs.readdir('./messages', (err, files) => {
        messages = files.map(file => {
          return JSON.parse(fs.readFileSync(`./messages/${file}`, 'utf8'))
        })
      })
    } catch (e) {
      messages = [];
    }
  },
  getMessages () {
    return messages.slice(messages.length - 5)
  },
  addMessage(message) {
    messages.push(message);
    this.save(message);
  },
  save(message) {
    const date = new Date().toISOString();
    fs.writeFileSync(`./messages/${date}.txt`, JSON.stringify({message, date: date}));
  }
};
